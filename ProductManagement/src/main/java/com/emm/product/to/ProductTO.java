package com.emm.product.to;

import java.io.Serializable;
import java.util.ArrayList;

import com.ab.ah.scad.acl.error.AttributeNotFoundException;
import com.ab.ah.scad.acl.error.TypeMismatchException;
import com.ab.ah.scad.acl.to.TransferObject;
import com.emm.product.to.field.ProductTOField;

//@XmlRootElement
public class ProductTO extends TransferObject implements Serializable{

	private Long ID;

	private String upc;

	private String ndc;

	private String drugName;

	private String quickCode;

	private String strength;

	private String wic;

	private Boolean active;

	private String quantity;

	private String size;

	private String gpa;

	private String dosageForm;

	public Long getID() {
		return ID;
	}

	public String getUpc() {
		return upc;
	}

	public String getNdc() {
		return ndc;
	}

	public String getDrugName() {
		return drugName;
	}

	public String getQuickCode() {
		return quickCode;
	}

	public String getStrength() {
		return strength;
	}

	public String getWic() {
		return wic;
	}

	public Boolean getActive() {
		return active;
	}

	public String getQuantity() {
		return quantity;
	}

	public String getSize() {
		return size;
	}

	public String getGpa() {
		return gpa;
	}

	public String getDosageForm() {
		return dosageForm;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public void setNdc(String ndc) {
		this.ndc = ndc;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public void setQuickCode(String quickCode) {
		this.quickCode = quickCode;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public void setWic(String wic) {
		this.wic = wic;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public void setGpa(String gpa) {
		this.gpa = gpa;
	}

	public void setDosageForm(String dosageForm) {
		this.dosageForm = dosageForm;
	}

	public String[] getAttributeNames() {
		ArrayList<String> staticField = new ArrayList<String>();
		staticField.add(ProductTOField.ID);
		staticField.add(ProductTOField.NDC);
		staticField.add(ProductTOField.UPC);
		staticField.add(ProductTOField.GPA);
		staticField.add(ProductTOField.QUICKCODE);
		staticField.add(ProductTOField.DOSAGEFORM);
		staticField.add(ProductTOField.DRUGNAME);
		staticField.add(ProductTOField.QUANTITY);
		staticField.add(ProductTOField.ACTIVE);
		staticField.add(ProductTOField.SIZE);
		staticField.add(ProductTOField.STRENGTH);
		staticField.add(ProductTOField.WIC);
		return staticField.toArray(new String[staticField.size()]);
	}

	@Override
	public Object getAttribute(String attributeName) {
		if (attributeName.equals(ProductTOField.ID))
			return getID();
		if (attributeName.equals(ProductTOField.NDC))
			return getNdc();
		if (attributeName.equals(ProductTOField.UPC))
			return getUpc();
		if (attributeName.equals(ProductTOField.GPA))
			return getGpa();
		if (attributeName.equals(ProductTOField.QUICKCODE))
			return getQuickCode();
		if (attributeName.equals(ProductTOField.DOSAGEFORM))
			return getDosageForm();
		if (attributeName.equals(ProductTOField.DRUGNAME))
			return getDrugName();
		if (attributeName.equals(ProductTOField.QUANTITY))
			return getQuantity();
		if (attributeName.equals(ProductTOField.ACTIVE))
			return getActive();
		if (attributeName.equals(ProductTOField.SIZE))
			return getSize();
		if (attributeName.equals(ProductTOField.STRENGTH))
			return getStrength();
		if (attributeName.equals(ProductTOField.WIC))
			return getWic();
		throw new AttributeNotFoundException(this.getClass(), attributeName);
	}

	@Override
	public void setAttribute(String attributeName, Object attributeValue) {
		try{
		if (attributeName.equals(ProductTOField.ID))
			setID(attributeValue == null ? null : (Long) attributeValue);
		else
		if (attributeName.equals(ProductTOField.NDC))
			setNdc(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.UPC))
			setUpc(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.GPA))
			setGpa(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.QUICKCODE))
			setQuickCode(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.DOSAGEFORM))
			setDosageForm(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.DRUGNAME))
			setDrugName(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.QUANTITY))
			setQuantity(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.ACTIVE))
			setActive(attributeValue == null ? null : (Boolean) attributeValue);
		else
		if (attributeName.equals(ProductTOField.SIZE))
			setSize(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.STRENGTH))
			setStrength(attributeValue == null ? null : (String) attributeValue);
		else
		if (attributeName.equals(ProductTOField.WIC))
			setWic(attributeValue == null ? null : (String) attributeValue);
		else
		throw new AttributeNotFoundException(this.getClass(), attributeName);
		}
		catch(ClassCastException e){
			throw new TypeMismatchException(attributeValue, attributeName, e);
			
		}

	}

}
