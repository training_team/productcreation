package com.emm.product.to.field;

public interface ProductTOField {
		
		public static final String ID ="id";
		
		public static final String UPC="upc";
		
		public static final String NDC="ndc";
		
		public static final String DRUGNAME="drugName";
		
		public static final String QUICKCODE="quickCode";
		
		public static final String STRENGTH="strength";
		
		public static final String WIC="wic";
		
		public static final String ACTIVE="active";
		
		public static final String QUANTITY="quantity";
		
		public static final String SIZE="size";
		
		public static final String GPA="gpa";
		
		public static final String DOSAGEFORM="dosageForm";

	}
