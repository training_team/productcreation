package com.emm.product.service.api.presentation;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;
import com.emm.product.to.ProductTO;

public interface ProductManagementPresentationService {

	public ProductTO createProduct(ProductTO productTO) throws ApplicationFailureError, ProductException;
	
	public ProductListTO searchProduct(ProductSearchTO productSearchTO) throws ApplicationFailureError;
	
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError;
	
	public ProductTO updateProduct(ProductTO productTO) throws ProductException;
	
}

