package com.emm.product.service.impl.presentation.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.service.api.application.ProductManagementApplicationService;
import com.emm.product.to.ProductTO;



/**
 * 
 * Da qui iniettiamo con @autowired il service Application
 *
 */
public class ProductCreationPresentationActionImpl implements ProductCreationPresentationAction{
	
	@Autowired
	private ProductManagementApplicationService productManagementApplicationService;
	
	public ProductTO createProduct(ProductTO productTO) throws ProductException, ApplicationFailureError {

		
		return productManagementApplicationService.createProduct(productTO) ;
}

}
