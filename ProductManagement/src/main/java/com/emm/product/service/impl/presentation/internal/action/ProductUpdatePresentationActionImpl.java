package com.emm.product.service.impl.presentation.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.emm.product.businessError.ProductException;
import com.emm.product.service.api.application.ProductManagementApplicationService;
import com.emm.product.to.ProductTO;

public class ProductUpdatePresentationActionImpl implements
		ProductUpdatePresentationAction {

	@Autowired 
	private ProductManagementApplicationService productManagementApplicationService;
	
	public ProductTO updateProduct(ProductTO productTo) throws ProductException {

		return productManagementApplicationService.updateProduct(productTo);
	}

}
