package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

/**
 * 
 * Da qui L'application si divide in ApplyRule e ApplyLogic
 *
 */
public class ProductManagementCreateProductApplicationActionImpl implements ProductManagementCreateProductApplicationAction{
	@Autowired
	ProductManagementCreateProductApplyRule productManagementCreateProductApplyRule ;
    @Autowired
    ProductManagementCreateProductApplyLogic productManagementCreateProductApplyLogic ;
    
	public ProductTO createProduct(ProductTO productTO) throws ProductException, ApplicationFailureError {
		
		ProductTO productTOValidated = productManagementCreateProductApplyRule.createProducTO(productTO);

		ProductTO result = productManagementCreateProductApplyLogic.createProductTO(productTOValidated);
		
		return result;
	}

}
