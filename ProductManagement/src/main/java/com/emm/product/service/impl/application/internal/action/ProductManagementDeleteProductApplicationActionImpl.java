package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.to.ProductTO;

public class ProductManagementDeleteProductApplicationActionImpl implements ProductManagementDeleteProductApplicationAction{

	@Autowired
	private ProductManagementDeleteProductApplyLogic productManagementDeleteProductApplyLogic;

	
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError {
		productManagementDeleteProductApplyLogic.deleteProduct(productTO);
		
	}
	
}
