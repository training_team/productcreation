package com.emm.product.service.impl.application.internal.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.businessError.ProductFailureErrorCodes;
import com.emm.product.entity.service.api.ProductManagementEntityService;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;
import com.emm.product.to.ProductTO;

public class ProductManagementSearchProductApplyLogicImpl implements ProductManagementSearchProductApplyLogic{

	@Autowired
	private ProductManagementEntityService productManagementEntityService;
	public ProductListTO searchProductApplyLogic(ProductSearchTO productSearchTO) throws ApplicationFailureError {
	
		validateInput(productSearchTO);
		List<ProductTO> productTOList=productManagementEntityService.searchProduct(productSearchTO);
		ProductListTO productListTO=new ProductListTO();
		productListTO.setProductListTO(productTOList);
		return productListTO;
	}
	private void validateInput(ProductSearchTO productSearchTO) throws ApplicationFailureError {
	 if (productSearchTO==null){
		 throw new ApplicationFailureError(ProductFailureErrorCodes.ERROR_OBJECT_NULL);
	 }
		
	}

}
