package com.emm.product.service.impl.application.internal.action;

import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

public interface ProductManagementCreateProductApplyRule {

	ProductTO createProducTO(ProductTO productTO) throws ProductException;

}
