package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.emm.product.businessError.ProductException;
import com.emm.product.businessRule.ProductDrugNameCheckerBR;
import com.emm.product.businessRule.ProductNDCCheckerBR;
import com.emm.product.businessRule.ProductQuickCodeCheckerBR;
import com.emm.product.businessRule.ProductUPCCheckerBR;
import com.emm.product.businessRule.ProductWICCheckerBR;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

public class ProductManagementCreateProductApplyRuleImpl implements ProductManagementCreateProductApplyRule {

	@Autowired
	ProductDrugNameCheckerBR productDrugNameCheckerBR;

	@Autowired
	ProductNDCCheckerBR productNDCCheckerBR;

	@Autowired
	ProductQuickCodeCheckerBR productQuickCodeCheckerBR;

	@Autowired
	ProductUPCCheckerBR productUPCCheckerBR;

	@Autowired
	ProductWICCheckerBR productWICCheckerBR;

	public ProductTO createProducTO(ProductTO productTO) throws ProductException {

		productDrugNameCheckerBR.checkMandatory(productTO);

		productNDCCheckerBR.checkMandatory(productTO);

		productQuickCodeCheckerBR.checkMandatory(productTO);

		productUPCCheckerBR.checkMandatory(productTO);

		productWICCheckerBR.checkMandatory(productTO);

		return productTO;
	}

}


