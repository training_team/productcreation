package com.emm.product.service.impl.application.internal.action;

import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public class ProductManagementUpdateProductApplyRuleImpl implements
		ProductManagementUpdateProductApplyRule {

	
	public ProductTO validateTO(ProductTO productTo) throws ProductException {

		if (productTo==null) throw new ProductException("productTO non valido");
		
		return productTo;
	}

}
