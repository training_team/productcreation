package com.emm.product.service.impl.application.internal.action;

import com.ab.ah.scad.acl.Action;
import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public interface ProductManagementCreateProductApplicationAction extends Action{


	ProductTO createProduct(ProductTO productTO) throws ProductException, ApplicationFailureError;

}
