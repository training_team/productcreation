package com.emm.product.service.impl.application.internal.action;

import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public interface ProductManagementUpdateProductApplyRule {

	public ProductTO validateTO(ProductTO productTo) throws ProductException;
}
