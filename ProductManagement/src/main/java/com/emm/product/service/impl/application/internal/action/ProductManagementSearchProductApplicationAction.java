package com.emm.product.service.impl.application.internal.action;

import com.ab.ah.scad.acl.Action;
import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;

public interface ProductManagementSearchProductApplicationAction extends Action {
	
	public ProductListTO searchProduct(ProductSearchTO productSearchTO) throws ApplicationFailureError;

}
