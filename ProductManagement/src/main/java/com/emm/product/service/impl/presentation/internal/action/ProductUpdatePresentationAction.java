package com.emm.product.service.impl.presentation.internal.action;

import com.ab.ah.scad.acl.Action;
import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public interface ProductUpdatePresentationAction extends Action{
	
	public ProductTO updateProduct(ProductTO productTo) throws ProductException;

}
