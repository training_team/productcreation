package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.entity.service.api.ProductManagementEntityService;
import com.emm.product.to.ProductTO;

public class ProductManagementDeleteProductApplyLogicImpl implements ProductManagementDeleteProductApplyLogic{
	@Autowired
	private ProductManagementEntityService productManagementEntityService;
	
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError {
		productManagementEntityService.deleteProduct(productTO);
	}
	
}
