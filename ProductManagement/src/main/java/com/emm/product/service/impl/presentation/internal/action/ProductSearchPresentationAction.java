package com.emm.product.service.impl.presentation.internal.action;

import java.util.List;

import com.ab.ah.scad.acl.Action;
import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;

public interface ProductSearchPresentationAction extends Action{
	

	public ProductListTO searchProduct(ProductSearchTO productSearchTO) throws ApplicationFailureError;

		
}
