package com.emm.product.service.impl.application.internal.action;

import com.emm.product.to.ProductTO;

public interface ProductManagementUpdateProductApplyLogic {

	public ProductTO updateProduct(ProductTO product);
}
