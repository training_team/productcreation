package com.emm.product.service.impl.application.internal.action;

import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public interface ProductManagementUpdateProductApplicationAction {

	public ProductTO updateProduct(ProductTO productTO) throws ProductException;
	
}
