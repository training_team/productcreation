package com.emm.product.service.impl.application.internal.action;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.to.ProductTO;

public interface ProductManagementCreateProductApplyLogic {

	ProductTO createProductTO(ProductTO productTOValidated) throws ApplicationFailureError;

}
