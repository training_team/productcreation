package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.emm.product.entity.service.api.ProductManagementEntityService;
import com.emm.product.to.ProductTO;


public class ProductManagementUpdateProductApplyLogicImpl implements
		ProductManagementUpdateProductApplyLogic {

	@Autowired
	private ProductManagementEntityService productManagementEntityService;

	public ProductTO updateProduct(ProductTO product) {
		
		return productManagementEntityService.updateProduct(product);
	}

}
