package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.entity.service.api.ProductManagementEntityService;
import com.emm.product.to.ProductTO;

public class ProductManagementCreateProductApplyLogicImpl implements ProductManagementCreateProductApplyLogic{

	@Autowired
	private ProductManagementEntityService productManagementEntityService;
	
	
	public ProductTO createProductTO(ProductTO productTOValidated) throws ApplicationFailureError {
		
		
		return productManagementEntityService.createProduct(productTOValidated);

	}

}
