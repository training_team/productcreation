package com.emm.product.service.impl.application.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.service.api.application.ProductManagementApplicationService;
import com.emm.product.service.impl.application.internal.action.ProductManagementDeleteProductApplicationAction;
import com.emm.product.service.impl.application.internal.action.ProductManagementUpdateProductApplicationAction;
import com.emm.product.service.impl.application.internal.action.ProductManagementCreateProductApplicationAction;
import com.emm.product.service.impl.application.internal.action.ProductManagementSearchProductApplicationAction;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;
import com.emm.product.to.ProductTO;

public class ProductManagementApplicationServiceImpl implements ProductManagementApplicationService{
	
	@Autowired
	private ProductManagementCreateProductApplicationAction  productManagementCreateProductApplicationAction;
	
	@Autowired
	private ProductManagementSearchProductApplicationAction productManagementSearchProductApplicationAction;
	
	@Autowired
	private ProductManagementUpdateProductApplicationAction producaManagementUpdateProductApplicationAction;
	
	@Autowired
	private ProductManagementDeleteProductApplicationAction  productManagementDeleteProductApplicationAction;
	
	public ProductTO createProduct(ProductTO productTO) throws ProductException, ApplicationFailureError {
		
		return productManagementCreateProductApplicationAction.createProduct(productTO) ;
	}
	
	public ProductListTO searchProduct(ProductSearchTO productSearchTO) throws ApplicationFailureError {
		
		return productManagementSearchProductApplicationAction.searchProduct(productSearchTO);
	}

	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError {
		productManagementDeleteProductApplicationAction.deleteProduct(productTO);		
	}

	public ProductTO updateProduct(ProductTO productTO) throws ProductException {
		
		return producaManagementUpdateProductApplicationAction.updateProduct(productTO);
		
		
	}


}
