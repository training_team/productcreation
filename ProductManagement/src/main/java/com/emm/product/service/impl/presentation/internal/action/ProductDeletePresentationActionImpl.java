package com.emm.product.service.impl.presentation.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.service.api.application.ProductManagementApplicationService;
import com.emm.product.to.ProductTO;

public class ProductDeletePresentationActionImpl implements ProductDeletePresentationAction{

	@Autowired
	private ProductManagementApplicationService productManagementApplicationService;
	
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError {
		productManagementApplicationService.deleteProduct(productTO);
		
	}

}
