package com.emm.product.service.impl.presentation.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.service.api.presentation.ProductManagementPresentationService;
import com.emm.product.service.impl.presentation.internal.action.ProductCreationPresentationAction;
import com.emm.product.service.impl.presentation.internal.action.ProductSearchPresentationAction;
import com.emm.product.service.impl.presentation.internal.action.ProductDeletePresentationAction;
import com.emm.product.service.impl.presentation.internal.action.ProductUpdatePresentationAction;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;
import com.emm.product.to.ProductTO;

public class ProductManagementPresentationServiceImpl implements ProductManagementPresentationService{
	@Autowired
	private ProductCreationPresentationAction productCreationPresentationAction;

	@Autowired
	private ProductSearchPresentationAction productSearchPresentationAction;

	@Autowired
	private ProductDeletePresentationAction productDeletePresentationAction;

	@Autowired
	private ProductUpdatePresentationAction productUpdatePresentationAction;

	public ProductTO createProduct(ProductTO productTO) throws ApplicationFailureError, ProductException {

		return productCreationPresentationAction.createProduct(productTO);
	}

	public ProductListTO searchProduct(ProductSearchTO productSearchTO) throws ApplicationFailureError {

		return productSearchPresentationAction.searchProduct(productSearchTO);

	}

	public void deleteProduct(ProductTO productTO)
			throws ApplicationFailureError {

		productDeletePresentationAction.deleteProduct(productTO);

	}

	public ProductTO updateProduct(ProductTO productTO) throws ProductException {

		return productUpdatePresentationAction.updateProduct(productTO);
		
	}


}
