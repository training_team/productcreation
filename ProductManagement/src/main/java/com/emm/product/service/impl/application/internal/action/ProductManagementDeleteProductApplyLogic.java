package com.emm.product.service.impl.application.internal.action;

import com.ab.ah.scad.acl.Action;
import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.to.ProductTO;

public interface ProductManagementDeleteProductApplyLogic extends Action {
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError;
}
