package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;


public class ProductManagementSearchProductApplicationActionImpl implements ProductManagementSearchProductApplicationAction{
	
	@Autowired
	ProductManagementSearchProductApplyLogic productManagementSearchProductApplyLogic;
	
	public ProductListTO searchProduct(ProductSearchTO productSearchTO) throws ApplicationFailureError {
		
		return productManagementSearchProductApplyLogic.searchProductApplyLogic(productSearchTO);
	}
	
	
	

}
