package com.emm.product.service.impl.presentation.internal.action;

import com.ab.ah.scad.acl.Action;
import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.to.ProductTO;

public interface ProductDeletePresentationAction extends Action{
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError;
}
