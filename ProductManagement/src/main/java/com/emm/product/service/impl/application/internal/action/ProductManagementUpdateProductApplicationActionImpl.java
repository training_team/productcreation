package com.emm.product.service.impl.application.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public class ProductManagementUpdateProductApplicationActionImpl implements
		ProductManagementUpdateProductApplicationAction {

	@Autowired
	private ProductManagementUpdateProductApplyRule productManagementUpdateProductApplyRule;
	
	@Autowired
	private ProductManagementUpdateProductApplyLogic productManagementUpdateProductApplyLogic;
	
	
	public ProductTO updateProduct(ProductTO productTO) throws ProductException {
		
		ProductTO validateProductTO = productManagementUpdateProductApplyRule.validateTO(productTO);
		
		ProductTO result = productManagementUpdateProductApplyLogic.updateProduct(validateProductTO);
		
		return result;
	}

}
