package com.emm.product.businessRule;

import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

public class ProductUPCCheckerBRImpl implements ProductUPCCheckerBR {

	public void checkMandatory(ProductTO productTO) throws ProductException {

		if(productTO.getUpc()==null || productTO.getUpc().trim().length()==0)
			throw new ProductException("UPC EXCEPTION: string null or empty");


		if(!productTO.getUpc().matches("[0-9]{3,11}"))	
			throw new ProductException("UPC EXCEPTION: input format not correct");

	}

}

