package com.emm.product.businessRule;

import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public class ProductQuickCodeCheckerBRImpl implements ProductQuickCodeCheckerBR {

	public void checkMandatory(ProductTO productTO) throws ProductException {
		
		String quickCode = productTO.getQuickCode();
		if (!(quickCode.matches("[a-zA-Z]{3}[0-9]{5}")) ) throw new ProductException("Valore non valido");
	}

}
