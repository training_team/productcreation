package com.emm.product.businessRule;

import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

public class ProductWICCheckerBRImpl implements ProductWICCheckerBR {

	public void checkMandatory(ProductTO productTO) throws ProductException {

		if(productTO == null || productTO.getActive() == false || productTO.getWic().isEmpty()){
			throw new ProductException("Prodotto non valido");}

		boolean b = productTO.getWic().matches("\\d{3,6}");
		if(!b)
			throw new ProductException("Sintassi wic errata");
	}
}

