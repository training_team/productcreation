package com.emm.product.businessRule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

public class ProductNDCCheckerBRImpl implements ProductNDCCheckerBR {

	public void checkMandatory(ProductTO productTO) throws ProductException {

		if (productTO.getNdc() == null || productTO.getNdc().trim().length()==0){
			throw new ProductException("NDC ERROR: exception string null or empty");
		}

		if (!productTO.getNdc().matches("[0-9]{2,5}[-]{1}[0-9]*{1,4}[-]?{1}[0-9]*{1,2}")){
			throw new ProductException("NDC ERROR: format NOT CORRECT");
		}
	}

}
