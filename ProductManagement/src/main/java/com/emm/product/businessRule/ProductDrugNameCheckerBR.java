package com.emm.product.businessRule;

import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

public interface ProductDrugNameCheckerBR {
	public void checkMandatory(ProductTO productTO) throws ProductException;
}
