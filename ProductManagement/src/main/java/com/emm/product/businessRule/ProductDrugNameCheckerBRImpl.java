package com.emm.product.businessRule;

//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

import com.emm.product.businessError.ProductException;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

public class ProductDrugNameCheckerBRImpl implements ProductDrugNameCheckerBR {

	public void checkMandatory(ProductTO productTO) throws ProductException {

		if(productTO.getDrugName()==null || productTO.getDrugName().trim().length()==0)
			throw new ProductException("DRUG NAME EXCEPTION: string null or empty");



		if (!productTO.getDrugName().matches("[a-zA-Z]{3,}")){
			throw new ProductException("DRUG NAME EXCEPTION: input format not correct");
		}
	}

}
