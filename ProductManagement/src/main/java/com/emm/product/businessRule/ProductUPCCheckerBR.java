package com.emm.product.businessRule;

import com.emm.product.businessError.ProductException;
import com.emm.product.to.ProductTO;

public interface ProductUPCCheckerBR {
	public void checkMandatory(ProductTO productTO) throws ProductException;

}
