package com.emm.product.businessError;

public class ProductException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1393429273267931354L;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ProductException(String message){
		super(message);
	}

}
