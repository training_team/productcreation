package com.emm.product.businessError;

public interface ProductFailureErrorCodes {

	public static String ERROR_BUSINESS_KEY_MISSING = "PFE-0001";

	public static String ERROR_OBJECT_NULL = "PFE-0002";

	public static String ERROR_FIELD_MISSING = "PFE-0003";

	public static String ERROR_PRODUCT_NULL = "PFE-0004";
}
