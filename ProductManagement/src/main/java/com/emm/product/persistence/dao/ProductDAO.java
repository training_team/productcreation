package com.emm.product.persistence.dao;

import com.ab.ah.scad.acl.dao.GenericDao;
import com.emm.product.persistence.model.jpa.Product;

public interface ProductDAO extends GenericDao<Product, Long> {

}
