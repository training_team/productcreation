package com.emm.product.persistence.model.jpa.defaultEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/*
 * Il nostro Bean ( per l'esercitazione ) 
 */
@Entity
public class Product extends com.emm.product.persistence.model.jpa.Product{

	@Id
	@GeneratedValue
	private Long ID;
	
	private String upc;
	
	private String ndc;
	
	private String drugName;
	
	private String quickCode;
	
	private String strength;
	
	private String wic;
	
	private Boolean active;
	
	private String quantity;
	
	private String size;
	
	private String gpa;
	
	private String dosageForm;
	
	public Product(){		
	}
	
	@Override
	public String getUpc() {
		return upc;
	}

	@Override
	public void setUpc(String upc) {
		this.upc = upc;
	}

	@Override
	public String getNdc() {
		return ndc;
	}

	@Override
	public void setNdc(String ndc) {
		this.ndc = ndc;
	}

	@Override
	public String getDrugName() {
		return drugName;
	}

	@Override
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	
	@Override
	public String getQuickCode() {
		return quickCode;
	}

	@Override
	public void setQuickCode(String quickCode) {
		this.quickCode = quickCode;
	}


	@Override
	public String getStrength() {
		return strength;
	}

	@Override
	public void setStrength(String strength) {
		this.strength = strength;
	}

	@Override
	public String getWic() {
		return wic;
	}

	@Override
	public void setWic(String wic) {
		this.wic = wic;
	}

	@Override
	public Boolean getActive() {
		return active;
	}

	@Override
	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String getQuantity() {
		return quantity;
	}

	@Override
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Override
	public String getSize() {
		return size;
	}

	@Override
	public void setSize(String size) {
		this.size = size;
	}

	@Override
	public String getGpa() {
		return gpa;
	}

	@Override
	public void setGpa(String gpa) {
		this.gpa = gpa;
	}

	@Override
	public String getDosageForm() {
		return dosageForm;
	}

	@Override
	public void setDosageForm(String dosageForm) {
		this.dosageForm = dosageForm;
	}

		
	@Override
	public String toString() {
		return "ProductTO [upc=" + upc + ", ndc=" + ndc + ", drugName="
				+ drugName + ", quickCode=" + quickCode + ", strength="
				+ strength + ", wic=" + wic + ", active=" + active
				+ ", quantity=" + quantity + ", size=" + size + ", gpa=" + gpa
				+ ", dosageForm=" + dosageForm + "]";
	}



	public String[] getAttributeNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getAttribute(String attributeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setAttribute(String attributeName, Object attributeValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {		
		return this.getClass().getName();
	}


public Long getId() {
	return this.ID;
}

public void setId(Long id) {
	this.ID=id;
}	
	


	
	
}
