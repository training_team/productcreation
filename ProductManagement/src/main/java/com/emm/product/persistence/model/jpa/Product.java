package com.emm.product.persistence.model.jpa;

import java.io.Serializable;

import com.ab.ah.scad.acl.model.AbstractStaticJpaDomainEntity;

public abstract class Product extends AbstractStaticJpaDomainEntity implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public abstract Long getId();

	public abstract void setId(Long id);
	
	public abstract String getUpc() ;

	public abstract String getNdc() ;

	public abstract String getDrugName();

	public abstract String getQuickCode();

	public abstract String getStrength() ;

	public abstract String getWic() ;

	public abstract Boolean getActive() ;

	public abstract String getQuantity() ;
	
	public abstract String getSize();

	public abstract String getGpa();

	public abstract String getDosageForm();

	public abstract void setUpc(String upc) ;
	
	public abstract void setNdc(String ndc) ;

	public abstract void setDrugName(String drugName);

	public abstract void setQuickCode(String quickCode);

	public abstract void setStrength(String strength) ;

	public abstract void setWic(String wic);

	public abstract void setActive(Boolean active);

	public abstract void setQuantity(String quantity);

	public abstract void setSize(String size);

	public abstract void setGpa(String gpa);

	public abstract void setDosageForm(String dosageForm);

	

}
