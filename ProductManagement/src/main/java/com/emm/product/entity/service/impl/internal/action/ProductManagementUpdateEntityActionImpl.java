package com.emm.product.entity.service.impl.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.emm.product.entity.service.impl.internal.converter.ConvertProductEntityToTO;
import com.emm.product.entity.service.impl.internal.converter.ConvertProductTOtoEntity;
import com.emm.product.persistence.dao.ProductDAO;
import com.emm.product.persistence.model.jpa.Product;
import com.emm.product.to.ProductTO;

public class ProductManagementUpdateEntityActionImpl implements
ProductManagementUpdateEntityAction {

	@Autowired
	private ProductDAO productDAO;
	@Autowired
	private ConvertProductTOtoEntity convertProductTOtoEntity;
	
	@Autowired
	private ConvertProductEntityToTO convertProductEntityToTO;

	public ProductTO updateProduct(ProductTO product) {

		//Convert ProductTO to Product
		Product productEntity = convertProductTOtoEntity.convert(product);
		
		//Create product on DB
		Product productDB = productDAO.updateDomainEntity(productEntity);
		
		//Convert Product to ProductTO
		ProductTO productResultTO=convertProductEntityToTO.convert(productDB);

		return productResultTO;
	}

}
