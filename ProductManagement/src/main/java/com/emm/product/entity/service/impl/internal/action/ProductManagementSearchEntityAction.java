package com.emm.product.entity.service.impl.internal.action;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.to.ProductListTO;
import com.emm.product.to.ProductSearchTO;

public interface ProductManagementSearchEntityAction {
	public ProductListTO searchProduct(ProductSearchTO productSearchTO)
			throws ApplicationFailureError ;

}
