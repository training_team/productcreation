package com.emm.product.entity.service.impl.internal.action;

import com.emm.product.to.ProductTO;

public interface ProductManagementUpdateEntityAction {

	public ProductTO updateProduct (ProductTO product );
}
