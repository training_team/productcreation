package com.emm.product.entity.service.impl.internal.action;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.persistence.model.jpa.Product;
import com.emm.product.to.ProductTO;

public interface ProductManagementCreateEntityAction {
	
	public ProductTO createProduct(ProductTO productTO)
			throws ApplicationFailureError;

}
