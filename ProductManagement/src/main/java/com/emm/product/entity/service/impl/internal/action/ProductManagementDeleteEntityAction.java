package com.emm.product.entity.service.impl.internal.action;

import com.ab.ah.scad.acl.Action;
import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.to.ProductTO;

public interface ProductManagementDeleteEntityAction extends Action {
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError;
}
