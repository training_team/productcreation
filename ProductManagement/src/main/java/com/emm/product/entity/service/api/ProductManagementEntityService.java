package com.emm.product.entity.service.api;

import java.util.List;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.ab.ah.scad.acl.service.stereotype.EntityService;
import com.emm.product.to.ProductSearchTO;
import com.emm.product.to.ProductTO;

@EntityService
public interface ProductManagementEntityService {

	public ProductTO createProduct(ProductTO productTO) throws ApplicationFailureError ;
	
	public List<ProductTO> searchProduct(ProductSearchTO productSearchTO);

	public ProductTO updateProduct(ProductTO product);
	
	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError;
	
	
}
