package com.emm.product.entity.service.impl.internal.converter;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.converter.DomainEntityConverter;
import com.emm.product.to.ProductTO;
import com.emm.product.persistence.model.jpa.Product;

public class ConvertProductEntityToTO {

	@Autowired
	private DomainEntityConverter domainEntityConverter;
	
	public ProductTO convert(
			Product productDB) {

		ProductTO productTO = null;
		if (productDB != null) {
			productTO = domainEntityConverter.convert(productDB, ProductTO.class);
		}
		return productTO;
	}

}
