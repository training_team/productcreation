package com.emm.product.entity.service.impl.internal.converter;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.converter.DomainEntityConverter;
import com.emm.product.persistence.model.jpa.Product;
import com.emm.product.to.ProductTO;

public class ConvertProductTOtoEntity {

	@Autowired
	private DomainEntityConverter domainEntityConverter;

	public Product convert(ProductTO productTO) {

		Product product = null;
		if (productTO != null) {
			product = domainEntityConverter.convert(productTO, Product.class);
		}
		return product;
	}
}
