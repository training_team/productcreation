package com.emm.product.entity.service.impl.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.entity.service.api.ProductManagementEntityService;
import com.emm.product.entity.service.impl.internal.action.ProductManagementCreateEntityAction;
import com.emm.product.entity.service.impl.internal.action.ProductManagementDeleteEntityAction;
import com.emm.product.entity.service.impl.internal.action.ProductManagementUpdateEntityAction;
import com.emm.product.to.ProductSearchTO;
import com.emm.product.to.ProductTO;


@Transactional
public class ProductManagementEntityServiceImpl implements ProductManagementEntityService {

	@Autowired
	private ProductManagementCreateEntityAction productManagementCreateEntityAction;
	@Autowired
	private ProductManagementDeleteEntityAction productManagementDeleteEntityAction;

	@Autowired
	private ProductManagementUpdateEntityAction productManagementUpdateEntityAction;

	public ProductTO createProduct(ProductTO productTO) throws ApplicationFailureError {

		return productManagementCreateEntityAction.createProduct(productTO);

	}

	public List<ProductTO> searchProduct(ProductSearchTO productSearchTO) {

		return null;
	}

	public void deleteProduct(ProductTO productTO) throws ApplicationFailureError {
		productManagementDeleteEntityAction.deleteProduct(productTO);

	}

	public ProductTO updateProduct(ProductTO product) {

		return productManagementUpdateEntityAction.updateProduct(product);
	}



}
