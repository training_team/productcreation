package com.emm.product.entity.service.impl.internal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductFailureErrorCodes;
import com.emm.product.entity.service.impl.internal.converter.ConvertProductEntityToTO;
import com.emm.product.entity.service.impl.internal.converter.ConvertProductTOtoEntity;
import com.emm.product.persistence.dao.ProductDAO;
import com.emm.product.persistence.model.jpa.Product;
import com.emm.product.to.ProductTO;

public class ProductManagementCreateEntityActionImpl implements
		ProductManagementCreateEntityAction {
	
	@Autowired
	private ProductDAO productDAO;
	@Autowired
	private ConvertProductTOtoEntity convertProductTOtoEntity;
	@Autowired
	private ConvertProductEntityToTO convertProductEntityToTO;

	public ProductTO createProduct(ProductTO productTO)
			throws ApplicationFailureError {
		
		//Input validation
		validateInput(productTO);
		//Convert ProductTO to Product
		Product product=convertProductTOtoEntity.convert(productTO);
		//Create product on DB
		Product productDB=productDAO.createDomainEntity(product);
		//Convert Product to ProductTO
		ProductTO productResultTO=convertProductEntityToTO.convert(productDB);
		
		return productResultTO;
	}

	private void validateInput(ProductTO productTO) throws ApplicationFailureError {
		
		if (productTO == null) {
			throw new ApplicationFailureError(ProductFailureErrorCodes.ERROR_PRODUCT_NULL);
		} 
	}

	

	

}
