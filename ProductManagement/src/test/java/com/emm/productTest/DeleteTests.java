package com.emm.productTest;

import net.sourceforge.stripes.action.Before;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.service.api.presentation.ProductManagementPresentationService;
import com.emm.product.to.ProductTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:/work/developer/git/productcreation/ProductManagement/resources/Application-config.xml")
public class DeleteTests {
	@Autowired
	private ProductManagementPresentationService productManagementPresentationService;
	
	@Before
	public void createTest() throws ProductException, ApplicationFailureError{
		ProductTO pr = createProduct();
		productManagementPresentationService.createProduct(pr);
		deleteTest(pr);
	}
	
	@Test
	public void deleteTest(ProductTO productTO) throws ApplicationFailureError{
		productManagementPresentationService.deleteProduct(productTO);
		Assert.assertNull(productTO);
	}
	
	public ProductTO createProduct(){
		ProductTO prod = new ProductTO();
		prod.setActive(true);
		prod.setDosageForm("dosageForm");
		prod.setDrugName("Ippopotamo");
		prod.setGpa("gpa");
		prod.setNdc("55555-4444-22");
		prod.setQuantity("20");
		prod.setQuickCode("AA55555");
		prod.setSize("20");
		prod.setStrength("20");
		prod.setUpc("123467911");
		prod.setWic("123456");
		return prod;
	}
}
