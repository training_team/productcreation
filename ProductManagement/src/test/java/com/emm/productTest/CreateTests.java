package com.emm.productTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ab.ah.scad.acl.error.ApplicationFailureError;
import com.emm.product.businessError.ProductException;
import com.emm.product.service.api.presentation.ProductManagementPresentationService;
import com.emm.product.to.ProductTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:/work/developer/git/productcreation/ProductManagement/resources/Application-config.xml")
public class CreateTests {

	@Autowired
	private ProductManagementPresentationService productManagementPresentationService;
	@Before
	public void setDB(){
		
	}
	@Test
	public void createTest() throws ProductException, ApplicationFailureError{
		ProductTO pr = createProduct();
		productManagementPresentationService.createProduct(pr);
	}
	
	@Test(expected=ProductException.class)
	public void DrugNameTest() throws ProductException, ApplicationFailureError{
		ProductTO pr = createProduct();
		pr.setDrugName("DrugName&%$£       ");
		productManagementPresentationService.createProduct(pr);
	}
	
//	@Test(expected=ProductException.class)
	@Test
	public void NDCTest() throws ProductException, ApplicationFailureError{
		ProductTO pr = createProduct();
		pr.setNdc("55555-444-");
		productManagementPresentationService.createProduct(pr);
	}
//	@Test
	@Test(expected=ProductException.class)
	public void UPCTest() throws ProductException, ApplicationFailureError{
		ProductTO pr = createProduct();
		pr.setUpc("12A");
		productManagementPresentationService.createProduct(pr);
	}
	
	
	@Test(expected=ProductException.class)
	public void quickCodeWrongTest() throws ProductException, ApplicationFailureError{
		ProductTO pr = createProduct();
		pr.setQuickCode("A123");
		productManagementPresentationService.createProduct(pr);
	}
	
	public ProductTO createProduct(){
		ProductTO prod = new ProductTO();
		prod.setActive(true);
		prod.setDosageForm("dosageForm");
		prod.setDrugName("Ippopotamo");
		prod.setGpa("gpa");
		prod.setNdc("55555-4444-22");
		prod.setQuantity("20");
		prod.setQuickCode("AA55555");
		prod.setSize("20");
		prod.setStrength("20");
		prod.setUpc("123467911");
		prod.setWic("123456");
		return prod;
	}
}
