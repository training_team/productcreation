package com.emm.productTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.emm.product.businessError.ProductException;
import com.emm.product.businessRule.ProductWICCheckerBR;
import com.emm.product.persistence.model.jpa.defaultEntity.Product;
import com.emm.product.to.ProductTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:/work/developer/git/productcreation/ProductManagement/resources/Application-config.xml")
public class WicBRTests {
	
	@Autowired
	private ProductWICCheckerBR productWICCheckerBR;
	
	@Test
	public void TestWic() throws ProductException{
		ProductTO prod = createProduct();
			productWICCheckerBR.checkMandatory(prod);
			//Assert.assert
	}
	
	public ProductTO createProduct(){
		ProductTO prod = new ProductTO();
		prod.setActive(true);
		prod.setDosageForm("dosageForm");
		prod.setDrugName("DrugName");
		prod.setGpa("gpa");
		prod.setNdc("ndc");
		prod.setQuantity("quantity");
		prod.setQuickCode("quickCode");
		prod.setSize("size");
		prod.setStrength("strenght");
		prod.setUpc("upc");
		prod.setWic("234561");
		
		return prod;
	}
}
